import React from 'react';
import Category from '../category/Category';
import Search from '../search/Search';

let INITIAL_ARRAY = [];

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const array = INITIAL_ARRAY;
    let searchTerm = event.target.value;
    let items = array.filter(o =>
      Object.keys(o).some(k => o[k].toLowerCase().includes(searchTerm.toLowerCase()))
    );
    this.setState({
      items: items
    });
  }

  componentDidMount() {
    fetch("https://the-cocktail-db.p.rapidapi.com/list.php?c=list", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
        "x-rapidapi-key": "2c17b6d805msh3e583ac7671962ap1dfc23jsnae3fbb8a82cc"
      }
    })
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items: result.drinks
        });
        INITIAL_ARRAY = result.drinks
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="CocktailsContainer">
          <Search handleChange={this.handleChange} />
          <ul>
            {items.map(item => (
              <Category key={item.strCategory} name={item.strCategory} />
            ))}
          </ul>
        </div>
      )
    }
  }
}

export default Categories;
