import React from 'react';
import './App.css';
import Categories from '../categories/Categories';

function App() {
  return (
    <Categories />
  );
}

export default App;
