import React from 'react';

class Category extends React.Component {
  render() {
    return (
      <li key={this.props.name}>
        {this.props.name}
      </li>
    )
  }
}
export default Category;
