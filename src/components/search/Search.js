import React from 'react';

class Search extends React.Component {
  render() {
    return (
      <input type='text' placeholder="Search" onChange={this.props.handleChange} />
    )
  }
}

export default Search;
